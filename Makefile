-include .env

SHELL := /bin/bash

PROJECTNAME := hbaas-server
DOCKERREGISTRY := uk.icr.io/dave-org

# Go related variables.
GOBASE := $(shell pwd)
GOPATH := $(GOBASE)/.go-pkg:$(GOBASE)
GOBIN := $(GOBASE)/.go-bin
GOFILES := $(wildcard *.go)

PACKAGENAME := $(shell go list)

VERSION ?= $(shell git describe --tags --always)
BUILDTIME := $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")

LDFLAGS := -ldflags "-X '$(PACKAGENAME)/version.Version=$(VERSION)' \
                     -X '$(PACKAGENAME)/version.BuildTime=$(BUILDTIME)'"

# Make is verbose in Linux. Make it silent.
MAKEFLAGS += --silent

IS_INTERACTIVE := $(shell [ -t 0 ] && echo 1)

ifdef IS_INTERACTIVE
LOG_INFO := $(shell tput setaf 12)
LOG_ERROR := $(shell tput setaf 9)
LOG_END := $(shell tput sgr0)
endif

define log
echo -e "$(LOG_INFO)⇛ $(1)$(LOG_END)"
endef

define log-error
echo -e "$(LOG_ERROR)⇛ $(1)$(LOG_END)"
endef

default: build

## build: Build the server executable.
build: code-gen
	$(call log,Building binary...)
	GOPATH=$(GOPATH) GOBIN=$(GOBIN) go build $(LDFLAGS) || (\
	    $(call log-error,Failed to build $(PROJECTNAME).) \
	    && false \
	)

## build-linux: Build the server executable in the Linux ELF format.
build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 make build

## code-gen: Generate code before compilation, such as bundled data.
code-gen: download-dependencies
	$(call log,Generating code...)
	cd data && $(GOBIN)/go-bindata -pkg data . || (\
	    $(call log-error,Unable to build data package.) \
	    && false \
	)

## download-dependencies: Download all library and binary dependencies.
download-dependencies:
	$(call log,Downloading dependencies...)
	GOPATH=$(GOPATH) GOBIN=$(GOBIN) go mod download
	test -e $(GOBIN)/go-bindata || GOPATH=$(GOPATH) GOBIN=$(GOBIN) go get github.com/kevinburke/go-bindata/...

## build-image: Build Docker container image for API.
build-image:
	$(call log,Building Docker image...)
	docker build \
	    --build-arg version=$(VERSION) \
	    --tag $(DOCKERREGISTRY)/$(PROJECTNAME):$(VERSION) \
	    --tag $(DOCKERREGISTRY)/$(PROJECTNAME):latest . || \
	(\
	    $(call log-error,Unable to build Docker image.) \
	    && false \
	)

## upload-image: Build Docker image and upload to private registry.
upload-image: build-image
	$(call log,Uploading Docker image...)
	ibmcloud cr login
	docker push $(DOCKERREGISTRY)/$(PROJECTNAME):$(VERSION) || \
	(\
	    $(call log-error,Unable to deploy Docker image to repository.) \
	    && false \
	)
	docker push $(DOCKERREGISTRY)/$(PROJECTNAME):latest || \
	(\
	    $(call log-error,Unable to deploy Docker image to repository.) \
	    && false \
	)

.PHONY: clean
## clean: Clean up all build files.
clean:
	@-rm $(OUTBINDIR)/$(PROJECTNAME) 2> /dev/null
	GOPATH=$(GOPATH) GOBIN=$(GOBIN) go clean
	@-rm ./**/bindata.go 2> /dev/null

.PHONY: help
all: help
help: Makefile
	echo
	echo "Choose a command run in "$(PROJECTNAME)":"
	echo
	sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	echo
