# Builder stage
# =============

# We use the standard Golang image as our "base image" (see terminology above
# for what that means).
FROM golang:latest AS builder

ARG version
ENV VERSION=$version

WORKDIR /app

COPY Makefile .
COPY go.mod .
COPY go.sum .
RUN make download-dependencies

# Copy all of the code in repository into this image so that we can build it.
COPY . .

# This runs the make command when *building* the image.
RUN make build-linux

# Runner stage
# ============

FROM alpine

COPY --from=builder /app/hbaas-server /app/

# Required for some cloud providers for remote access.
RUN apk add bash

# Document that our service listens on port 8000
EXPOSE 8000

CMD ["/app/hbaas-server"]
