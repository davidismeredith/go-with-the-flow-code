package router

import (
	"github.com/labstack/echo/v4"
	echoMiddleware "github.com/labstack/echo/v4/middleware"
	"hartree.stfc.ac.uk/hbaas-server/context"
	"hartree.stfc.ac.uk/hbaas-server/handlers"
)

func New(appContext context.Context) *echo.Echo {
	e := echo.New()

	e.Pre(echoMiddleware.RemoveTrailingSlash())

	apiGroup := e.Group("/")

	handlers.RegisterHandlers(apiGroup, appContext)

	return e
}
