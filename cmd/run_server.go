package cmd

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"hartree.stfc.ac.uk/hbaas-server/context"

	"github.com/gocarina/gocsv"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"golang.org/x/crypto/acme/autocert"

	"hartree.stfc.ac.uk/hbaas-server/data"
	"hartree.stfc.ac.uk/hbaas-server/router"
	"hartree.stfc.ac.uk/hbaas-server/version"
)

type DateOfBirth struct {
	time.Time
}

func (d *DateOfBirth) UnmarshalCSV(csv string) (err error) {
	d.Time, err = time.Parse("2 January 2006", csv)
	return
}

type Person struct {
	Name      string      `csv:"name"`
	BirthDate DateOfBirth `csv:"birth_date"`
}

func init() {
	rootCmd.AddCommand(runServerCmd)
}

var runServerCmd = &cobra.Command{
	Use:   "run-server",
	Short: "Run the HBaaS API server.",
	Long:  "Run the HBaaS API server providing a RESTful interface to all birthday greeting functionality.",
	Run:   runServer,
}

func runServer(_ *cobra.Command, _ []string) {
	peopleByBirthday := populatePeople()
	appContext := context.Context{PeopleByBirthday: peopleByBirthday}

	env := viper.GetString("env")
	if env == "" {
		env = "dev"
		log.Println(fmt.Sprintf("Environment not set, so defaulting to %s", env))
	}
	if env != "dev" && env != "prod" {
		log.Fatal("Invalid environment:", env, ", expected either 'dev' or 'prod'.")
	}

	log.Println(fmt.Sprintf("Running server %s, built %s.", version.Version, version.BuildTime))
	log.Println(fmt.Sprintf("Environment is: %s.", env))

	port, err := strconv.Atoi(viper.GetString("port"))
	if err != nil {
		port = 8000
		log.Println(fmt.Sprintf("Port not set, so defaulting to %d.", port))
	}

	e := router.New(appContext)

	// Middleware
	e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	if env == "prod" {
		e.Pre(middleware.HTTPSRedirect())
	}

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", port)))
}

func populatePeople() map[context.BirthDay][]string {
	peopleCsvBytes, err := data.Asset("people.csv")
	if err != nil {
		log.Fatal("Unable to read people data:", err)
	}
	peopleCsv := string(peopleCsvBytes)

	var people []*Person
	if err := gocsv.UnmarshalString(peopleCsv, &people); err != nil {
		log.Fatal("Unable to unmarshal people from CSV:", err)
	}

	peopleByBirthday := make(map[context.BirthDay][]string)

	for _, person := range people {
		birthDay := context.NewBirthDay(person.BirthDate.Time)

		peopleWithBirthday, exists := peopleByBirthday[birthDay]
		if !exists {
			peopleWithBirthday = []string{}
		}

		peopleByBirthday[birthDay] = append(peopleWithBirthday, person.Name)
	}

	return peopleByBirthday
}
